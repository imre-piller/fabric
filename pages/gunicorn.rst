Gunicorn
========

Homepage: http://gunicorn.org/

Installation
------------

.. code-block:: shell

   pip install gunicorn


.. tip::

   Also can be installed with ``requirements.txt`` file.


