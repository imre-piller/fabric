Vue.js
======

VueJS Guide: https://vuejs.org/v2/guide/


NodeJS installation on Debian
-----------------------------

Adding NodeJS PPA (*Personal Package Archive*):

.. code-block:: shell

   # apt-get install curl
   $ curl -sL https://deb.nodesource.com/setup_9.x > node_install.sh
   # sh node_install.sh


Installing NodeJS and NPM:

.. code-block:: shell

   # apt-get install nodejs


Check NodeJS version:

.. code-block:: shell

   $ node -v
   $ npm -v

